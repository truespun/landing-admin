import React, {useEffect} from 'react';
import './App.css';
import { Admin, Resource, ShowGuesser, } from 'react-admin';
import { ContactConfig , EditContactConfig, CreateContactConfig} from './contact-config'
import { RequestList} from './contact-request'
import myDataProvider from './myDataProvider'
import {fetchJson as httpClient} from './httpClient'
import authProvider from './authProvider';
import config from './config';

const dataProvider = myDataProvider(`${config.protocol}`, httpClient);
const App = () => {
  return (
    <Admin dataProvider={dataProvider} authProvider={authProvider}>
      <Resource name="contact-requests" list={RequestList} show={ShowGuesser}/>
      <Resource name="contact-config" list={ContactConfig} show={ShowGuesser} edit={EditContactConfig} create={CreateContactConfig} />
    </Admin>
  )
}

export default App;
