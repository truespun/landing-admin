import React from 'react';
import { List, Datagrid, TextField, EmailField, RichTextField} from 'react-admin';


export const RequestList = props => (
    <List {...props} title="Контактные данные клинтов">
        <Datagrid>
            <TextField label="Имя" source="name" />
            <EmailField label="Эл.почта" source="email" />
            <TextField label="Телефон" source="phone" />
            <TextField label="Цена Авто" source="autoValue"/>
            <TextField label="Год Выпуска" source="year"/>
            <TextField label="Объем двигателя" source="engine"/>
            <TextField label="Тип двигателя" source="typeEngine"/>
            <TextField label="Сумма USD" source="sum"/>

        </Datagrid>
    </List>
);

