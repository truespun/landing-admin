let configOverrides = {};

if (typeof process.env.REACT_APP_API_PROTOCOL === 'string') {
  configOverrides.protocol = process.env.REACT_APP_API_PROTOCOL;
}

let config = Object.assign({}, configOverrides);

export default config;
