import React from 'react';
import { List, Datagrid, TextField,  EmailField, SimpleForm, TextInput, Edit,Create,UrlField} from 'react-admin';



export const ContactConfig = props => (
    <List {...props} title={"Наши контакты"}>
        <Datagrid rowClick="edit">
            <TextField source="phone" label="Телефон" />
            <EmailField source="email" label="Эл.почта" />
            <TextField source="address" label="Адрес" />
            <UrlField source="facebook" label="facebook"/>
            <UrlField source="instagram" label="instagram"/>
        </Datagrid>
    </List>
)



export const EditContactConfig = props => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput source="phone" />
            <TextInput source="email" />
            <TextInput source="address" />
            <TextInput source="facebook" />
            <TextInput source="instagram" />
        </SimpleForm>
    </Edit>
);

export const CreateContactConfig = props => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="phone" />
            <TextInput source="email" />
            <TextInput source="address" />
            <TextInput source="facebook" />
            <TextInput source="instagram" />
        </SimpleForm>
    </Create>
);

